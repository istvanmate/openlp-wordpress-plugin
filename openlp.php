<?php
/**
* @package OpenLP-Wordpress-Plugin
*/
/*
* Plugin Name:  OpenLP Wordpress Plugin
* Version:      0.0.2
* Author:       István Máté
* Plugin URI:   https://gitlab.com/istvanmate/openlp-wordpress-plugin
* Description:  This plugin is capable of showing OpenLP Song Data through a stage view.
*/

defined( 'ABSPATH' ) or die('Error');

register_activation_hook(__FILE__, 'openlpActivation');
register_deactivation_hook(__FILE__, 'openlpDeactivate');

add_action('rest_api_init', 'openlpAPIPaths');

function openlpAPIPaths() {
    //GET method to send in JSON all song's id, title, alternate title
    register_rest_route('openlp/v2', 'search', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'openlpSearchAllResult'
    ));
    //GET method to send in JSON matching song's id, title, alternate title
    register_rest_route('openlp/v2', 'search/(?P<string>.*)', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'openlpSearchResult'
    ));
    //POST method to send in JSON song data with matching id
    register_rest_route('openlp/v2', 'controller/live-items(.*)', array(
        'methods' => WP_REST_Server::CREATABLE,
        'callback' => 'openlpOpenSongById'
    ));
    //POST method to receive in JSON song updates
    register_rest_route('openlp/v2', 'sync/upload', array(
        'methods' => WP_REST_Server::CREATABLE,
        'callback' => 'openlpSyncUpload'
    ));
    //GET method to send in JSON all song's id, last_modified
    register_rest_route('openlp/v2', 'sync/list', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'openlpSyncList'
    ));
    register_rest_route('openlp/v2', 'service/get', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'openlpReturnDefaultService'
    ));
    register_rest_route('openlp/v2', 'service/get/(?P<id>.*)', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'openlpReturnServiceById'
    ));
    register_rest_route('openlp/v2', 'service/set', array(
        'methods' => WP_REST_Server::CREATABLE,
        'callback' => 'openlpUpdateService'
    ));
}

function openlpSearchAllResult() {
    global $wpdb;
    $openlp_songs_table = $wpdb -> prefix.'openlp_songs';
    $result = $wpdb->get_results ( "SELECT id, title, alternate_title FROM $openlp_songs_table ORDER BY title ASC" );
    $searchAllResult = array();
    foreach ($result as $song) {
        $searchAllResult[] = array(
            (int)$song->id,
            $song->title,
            !is_null($song->alternate_title) ? $song->alternate_title : ""
        );
    }
    return $searchAllResult;
}

function openlpSearchResult($request) {
    global $wpdb;
    $openlp_songs_table = $wpdb -> prefix.'openlp_songs';
    $searchString = rawurldecode($request['string']);
    $result = $wpdb->get_results ( "SELECT id, title, alternate_title FROM $openlp_songs_table WHERE search_lyrics LIKE '%$searchString%' OR title LIKE '%$searchString%' OR alternate_title LIKE '%$searchString%' ORDER BY title ASC" );
    $searchAllResult = array();
    foreach ($result as $song) {
        $searchAllResult[] = array(
            (int)$song->id,
            $song->title,
            !is_null($song->alternate_title) ? $song->alternate_title : ""
        );
    }
    return $searchAllResult;
}

function openlpOpenSongById($request) {
    $params = $request->get_params();
    if (!key_exists('id',$params)) {
        return 'Error';
    }
    $id = (int)$params['id'];
    $custom_formatting_tags = array(
        "[","]",
        "{score}", "{/score}",
        "{notes}", "{/notes}",
        "{warn}", "{/warn}",
        "{br}", "{nobr}",
        "{lbl}", "{/lbl}",
        "{lr}", "{/lr}", 
        "{it}", "{/it}",
        "{u}", "{/u}",
        "{lt}", "{/lt}",
        "\n", "{cnl}",
        "{musescore}", "{/musescore}"
    );
    $custom_formatting_htmls = array(
        '<span class="chord"><span><strong>', '</strong></span></span>',
        '<div id="sheet" style="display:none">', '</div>',
        '<span class="notes" style="display:none">', '</span>',
        '<span class="worshipbandwarning">', '</span>',
        '<br>', '<nobr />',
        '<span style="-webkit-text-fill-color:#7F7FFF">', '</span>',
        '<span style="-webkit-text-fill-color:#FF7F7F">', '</span>',
        '<em>', '</em>',
        '<span style="text-decoration: underline">', '</span>',
        '<span class="lt" style="display:none">', '</span>',
        '<br>', '<br>',
        '<div id="musescore" style="display:none">', '</div>'
    );
    global $wpdb;
    $openlp_songs_table = $wpdb->prefix.'openlp_songs';
    $song = $wpdb->get_row( "SELECT * FROM $openlp_songs_table WHERE id = $id" );
    $slides = array();
    $xmlDom = new DOMDocument('1.0', 'UTF-8');
    $xmlDom->loadXML($song->lyrics);
    $lyricsArray = xml_to_array($xmlDom);
    if (strlen($song->verse_order) > 0) {
        $verses = array();
        foreach ($lyricsArray['song']['lyrics']['verse'] as $verse) {
            $verseType = $verse['@attributes']['type'] . $verse['@attributes']['label'];
            $verses += array (
                $verseType => $verse['@cdata']
            );
        }
        foreach(explode(" ", $song->verse_order) as $verseType) {
            if (strlen($verseType) == 1) {
                $verseType .= "1";
            }
            $verse = $verses[$verseType];
            if (str_contains($verse, '[')) {
                $verse = '<span class="chordline">' . $verse . '</span>';
            } else {
                $verse = '<span class="nochordline">' . $verse . '</span>';
            }
            $slides[] = array(
                'chords' => str_replace($custom_formatting_tags, $custom_formatting_htmls, str_replace("[---]\n", "", str_replace("[--}{--]\n", "", $verse))),
                'title' => $song->title,
                'tag' => $verseType
            );
        }
    } else {
        foreach($lyricsArray['song']['lyrics']['verse'] as $verse) {
            if (str_contains($verse['@cdata'], '[')) {
                $verseWrap = '<span class="chordline">';
            } else {
                $verseWrap = '<span class="nochordline">';
            }
            $slides[] = array(
                'chords' => str_replace($custom_formatting_tags, $custom_formatting_htmls, str_replace("[---]\n", "", str_replace("[--}{--]\n", "", $verseWrap . $verse['@cdata'] . '</span>'))),
                'title' => $song->title,
                'tag' => $verse['@attributes']['type'] . $verse['@attributes']['label']
            );
        }
    }
    return array(
        'name' => 'songs',
        'slides' => $slides,
        'title' => $song->title
    );
}

function openlpReturnServiceById($request) {
    $id = (int)rawurldecode($request['id']);
    global $wpdb;
    $openlp_services_table = $wpdb -> prefix.'openlp_services';
    $result = $wpdb->get_results ( "SELECT service_json FROM $openlp_services_table WHERE id = $id" );
    if (count($result) == 1) {
        $service = array();
        $service_json = json_decode($result[0]->service_json, false);
        foreach ($service_json as $song) {
            $service[] = array(
                (int)$song->id,
                $song->title,
                $song->alternate_title
            );
        }
        return $service;
    } else {
        return 'Error';
    }
}

function openlpReturnDefaultService() {
    global $wpdb;
    $openlp_services_table = $wpdb -> prefix.'openlp_services';
    $result = $wpdb->get_results ( "SELECT service_json FROM $openlp_services_table WHERE id = 0" );
    if (count($result) == 1) {
        $service = array();
        $service_json = json_decode($result[0]->service_json, false);
        if (count($service_json) == 0) {
            $service = openlpSearchAllResult();
        } else {
            foreach($service_json as $song) {
                $service[] = array(
                    (int)$song->id,
                    $song->title,
                    $song->alternate_title
                );
            }
        }
        return $service;
    } else {
        return 'Error';
    }
}

function openlpUpdateService($request) {
    $params = $request->get_params();
    if (!key_exists('id',$params)) {
        return 'Error, no id';
    }
    global $wpdb;
    $openlp_services_table = $wpdb -> prefix.'openlp_services';
    $id = (int)$params['id'];
    //return $params;
    //$service_json = (string)$params['service_json'];
    $result = $wpdb->get_results("SELECT * FROM $openlp_services_table WHERE id = $id");
    if (count($result) > 1) {
        return 'Error';
    }
    if (count($result) == 1) {
        $sqlResult = $wpdb->update($openlp_services_table, $params, array('id' => $id));
        return array('result' => "Ok. $sqlResult rows updated.");
    } else {
        $sqlResult = $wpdb->insert($openlp_services_table, $params);
        return array('result' => "Ok. $sqlResult rows inserted.");
    }
}

function openlpSyncUpload($request) {
    $params = $request->get_params();
    if (!key_exists('id',$params)) {
        return 'Error';
    }
    global $wpdb;
    $openlp_songs_table = $wpdb -> prefix.'openlp_songs';
    $id = (int)$params['id'];
    $result = $wpdb->get_results("SELECT * FROM $openlp_songs_table WHERE id = $id");
    if (count($result) > 1) {
        return 'Error';
    }
    $timestamp = json_decode('{"date": "'.$params['last_modified'].'"}', true);
    $timestamp = preg_replace( '/[^0-9]/', '', $timestamp['date']);
    $params['last_modified'] = date("Y-m-d H:i:s", $timestamp / 1000);
    $timestamp = json_decode('{"date": "'.$params['create_date'].'"}', true);
    $timestamp = preg_replace( '/[^0-9]/', '', $timestamp['date']);
    $params['create_date'] = date("Y-m-d H:i:s", $timestamp / 1000);
    if (count($result) == 1) {
        $song = $result[0];
        if ($params['last_modified'] > $song->last_modified) {
            #unset($params['id']);
            $sqlResult = $wpdb->update($openlp_songs_table, $params, array('id' => $id));
            return array('result' => "Ok. $sqlResult rows updated.");
        } else {
            return array('result' => "Ok. No update necessary.");
        }
    } else {
        $sqlResult = $wpdb->insert($openlp_songs_table, $params);
        return array('result' => "Ok. $sqlResult rows updated.");
    }
}

function openlpSyncList() {
    global $wpdb;
    $openlp_songs_table = $wpdb -> prefix.'openlp_songs';
    $result = $wpdb->get_results ( "SELECT id, last_modified FROM $openlp_songs_table" );
    return $result;
}

function openlpActivation() {
    global $wpdb;
    $openlp_songs_table = $wpdb -> prefix.'openlp_songs';
    $openlp_services_table = $wpdb -> prefix.'openlp_services';

    $sql_songs_table = "CREATE TABLE $openlp_songs_table (
        id INTEGER NOT NULL, 
        title VARCHAR(255) NOT NULL, 
        alternate_title VARCHAR(255), 
        lyrics TEXT NOT NULL, 
        verse_order VARCHAR(128), 
        copyright VARCHAR(255), 
        comments TEXT, 
        ccli_number VARCHAR(64), 
        theme_name VARCHAR(128), 
        search_title VARCHAR(255) NOT NULL, 
        search_lyrics TEXT NOT NULL, 
        create_date DATETIME, 
        last_modified DATETIME, 
        temporary BOOLEAN, 
        PRIMARY KEY (id)
    )";
    $sql_services_table = "CREATE TABLE $openlp_services_table (
        id INTEGER NOT NULL, 
        service_json TEXT,
        PRIMARY KEY (id)
    )";
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
    dbDelta($sql_songs_table);
    dbDelta($sql_services_table);
}

function openlpDeactivate() {
    global $wpdb;
    $openlp_songs_table = $wpdb -> prefix.'openlp_songs';
    $openlp_services_table = $wpdb -> prefix.'openlp_services';
    $sql_songs_table = $wpdb -> query("DROP TABLE $openlp_songs_table");
    $sql_services_table = $wpdb -> query("DROP TABLE $openlp_services_table");
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
    dbDelta($sql_songs_table);
    dbDelta($sql_services_table);
}

function xml_to_array($root) {
    $result = array();
    if ($root->hasAttributes()) {
        $attrs = $root->attributes;
        foreach ($attrs as $attr) {
            $result['@attributes'][$attr->name] = $attr->value;
        }
    }
    if ($root->hasChildNodes()) {
        $children = $root->childNodes;
        if ($children->length == 1) {
            $child = $children->item(0);
            switch ($child->nodeType) {
                case XML_TEXT_NODE:
                    $result['_value'] = $child->nodeValue;
                    return count($result) == 1
                        ? $result['_value']
                        : $result;
                case XML_CDATA_SECTION_NODE:
                    $result['@cdata'] = $child->nodeValue;
                    return count($result) == 1
                        ? $result['@cdata']
                        : $result;
            }
        }
        $groups = array();
        foreach ($children as $child) {
            if($child->nodeType == XML_TEXT_NODE && empty(trim($child->nodeValue))) continue;
            if (!isset($result[$child->nodeName])) {
                $result[$child->nodeName] = xml_to_array($child);
            } else {
                if (!isset($groups[$child->nodeName])) {
                    $result[$child->nodeName] = array($result[$child->nodeName]);
                    $groups[$child->nodeName] = 1;
                }
                $result[$child->nodeName][] = xml_to_array($child);
            }
        }
    }
    return $result;
}