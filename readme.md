This is a basic WordPress plugin (WordPress API engine) to serve ajax requests from a modified OpenLP stage view. Currently only the API engine is built, stageview files can be deployed anywhere near WordPress in their own directory. Later I will include the files with the plugin.

Currently I am syncing songs with PowerShell, requires PSSQLite:

```powershell
$url = "https://www.website.com"
$OpenLPDataDirectory = "Path\To\OpenLP\Data\Directory"
$data = Invoke-SqliteQuery -DataSource "$OpenLPDataDirectory\songs\songs.sqlite" -Query "SELECT * FROM songs"
$serversonglist = Invoke-RestMethod -Uri "$url/wp-json/openlp/v2/sync/list" -ContentType "application/json; charset=UTF-8" -Method Get
$i = 0
foreach ($song in $data) {
    Write-Progress -Activity "Updating Wordpress Database" -Status "Checking song $i of $($data.Count) ($($song.title))" -PercentComplete ($i * 100 / $data.Count)
    if ((($serversonglist | ? {$_.id -eq $song.id}) -eq $null) -or ((Get-Date "$(($serversonglist | ? {$_.id -eq $song.id}).last_modified)-00:00") -lt $song.last_modified)) {
        Write-Progress -Activity "Updating Wordpress Database" -Status "Updating song $i of $($data.Count) ($($song.title))" -PercentComplete ($i * 100 / $data.Count)
        Invoke-RestMethod -Uri "$url/wp-json/openlp/v2/sync/upload" -ContentType "application/json; charset=UTF-8" -Method POST -Body ([System.Text.Encoding]::UTF8.GetBytes((ConvertTo-Json $song)))
    }
    $i++
}
```